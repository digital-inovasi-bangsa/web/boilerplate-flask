from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required
from app.mod_dashboard import bp
from flask_babel import _
from ..decorators import confirmation


@bp.route("/")
@bp.route("/dashboard")
@login_required
@confirmation
def index():
    return render_template("main/dashboard.html", title=_("Dashboard"))


@bp.route("/blank")
@confirmation
@login_required
def blank():
    return render_template("main/blank_template.html", title=_("Blank Template"))


@bp.route("/docs/dashboard")
@confirmation
@login_required
def docs_dashboard():
    return render_template("main/theme_docs/dashboard.html", title=_("Dashboard Docs"))


@bp.route("/docs/charts")
@confirmation
@login_required
def docs_charts():
    return render_template("main/theme_docs/charts.html", title=_("Charts Docs"))


@bp.route("/docs/tables")
@confirmation
@login_required
def docs_tables():
    return render_template("main/theme_docs/tables.html", title=_("Tables Docs"))


@bp.route("/docs/forms")
@confirmation
@login_required
def docs_forms():
    return render_template("main/theme_docs/forms.html", title=_("Forms Docs"))


@bp.route("/docs/bs-element")
@confirmation
@login_required
def docs_bs_element():
    return render_template(
        "main/theme_docs/bs_element.html", title=_("Bootstrap Element Docs")
    )


@bp.route("/docs/bs-grid")
@confirmation
@login_required
def docs_bs_grid():
    return render_template(
        "main/theme_docs/bs_grid.html", title=_("Bootstrap Grid Docs")
    )

